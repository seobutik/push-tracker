package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	negroniprometheus "github.com/bloogrox/negroni-prometheus"
	"github.com/caarlos0/env"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"github.com/twinj/uuid"
	"github.com/urfave/negroni"

	"github.com/go-redis/redis"

	as "github.com/aerospike/aerospike-client-go"
)

const (
	deliveryQueue = "push-delivered"
	clickQueue    = "push_clicked"
	postbackQueue = "send-postback"
)

var (
	redisClient      *redis.Client
	asClient         *as.Client
	rabbitConn       *amqp.Connection
	rabbitCh         *amqp.Channel
	rabbitCloseError chan *amqp.Error
	log              *logrus.Logger
	cfg              config
)

type config struct {
	RedisURL       string   `env:"REDIS_URL"`
	RabbitMQURL    string   `env:"RABBITMQ_URL"`
	AerospikeHosts []string `env:"AEROSPIKE_HOSTS" envSeparator:","`
	AerospikePort  int      `env:"AEROSPIKE_PORT"`
	AerospikeNS    string   `env:"AEROSPIKE_NS"`
	AerospikeSet   string   `env:"AEROSPIKE_SET"`
	Debug          bool     `env:"DEBUG"`
}

func failOnError(err error) {
	if err != nil {
		log.Fatalf("%s", err)
	}
}

type actor struct {
	channel *amqp.Channel
	queue   string
}

func (a actor) tell(message string) error {
	log.Debug(fmt.Sprintf("Sending message %s to queue %s ", message, a.queue))
	err := a.channel.Publish(
		"",      // exchange
		a.queue, // routing key
		false,   // mandatory
		false,   // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		})
	return err
}

type message struct {
	MessageID        uuid.UUID
	QueueName        string
	ActorName        string
	Args             string
	MessageTimestamp int64
}

// TODO refactor to json.Marshal(struct)
func (msg message) serialized() string {
	j := `
	{
		"message_id": "%v",
		"queue_name": "%s",
		"actor_name": "%s",
		"args": %s,
		"kwargs": {},
		"options": {},
		"message_timestamp": %d
	}`

	return fmt.Sprintf(
		j,
		msg.MessageID,
		msg.QueueName,
		msg.ActorName,
		msg.Args,
		msg.MessageTimestamp,
	)
}

// IconHandler downloads image by url and serve it to client
func IconHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var (
		pushID   = vars["push-id"]
		keyRedis = fmt.Sprintf("pushes:%s:icon-url", pushID)
		keyAs    = fmt.Sprintf("push:%s", pushID)
		iconURL  string
	)

	// Aerospike
	policy := as.NewPolicy()
	key, err := as.NewKey(cfg.AerospikeNS, cfg.AerospikeSet, keyAs)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}
	rec, err := asClient.Get(policy, key)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}

	if rec != nil {
		iconURL = rec.Bins["icon-url"].(string)
		log.Debug("IconHandler: from aerospike")
	} else {
		val, err := redisClient.Get(keyRedis).Result()
		if err != nil {
			http.NotFound(w, r)
			return
		}

		iconURL = val
		log.Debug("IconHandler: from redis")
	}

	// download content
	timeout := time.Duration(10 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	response, err := client.Get(iconURL)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", response.Header.Get("Content-Type"))
	w.Write(body)

	t := time.Now()

	payload := fmt.Sprintf(`{"datetime": "%s"}`, t.UTC().Format(time.RFC3339))
	deliveryMessage := message{
		uuid.NewV4(),
		deliveryQueue,
		"persist_push_delivered",
		fmt.Sprintf(`["%s", %s]`, pushID, payload),
		t.UnixNano() / int64(time.Millisecond),
	}
	deliveryActor := actor{rabbitCh, deliveryQueue}
	err = deliveryActor.tell(deliveryMessage.serialized())
	if err != nil {
		log.Warn(err)
	}

	postbackMessage := message{
		uuid.NewV4(),
		postbackQueue,
		"send_postback",
		fmt.Sprintf(`["%s", "delivery"]`, pushID),
		t.UnixNano() / int64(time.Millisecond),
	}
	postbackActor := actor{rabbitCh, postbackQueue}
	err = postbackActor.tell(postbackMessage.serialized())
	if err != nil {
		log.Warn(err)
	}
}

// ClickHandler performs a redirect
func ClickHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var (
		pushID      = vars["push-id"]
		keyRedis    = fmt.Sprintf("pushes:%s:click-action", pushID)
		keyAs       = fmt.Sprintf("push:%s", pushID)
		redirectURL string
	)

	// Aerospike
	policy := as.NewPolicy()
	key, err := as.NewKey(cfg.AerospikeNS, cfg.AerospikeSet, keyAs)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}
	rec, err := asClient.Get(policy, key)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}

	if rec != nil {
		redirectURL = rec.Bins["click-action"].(string)
		log.Debug("ClickHandler: from aerospike")
	} else {
		clickURL, err := redisClient.Get(keyRedis).Result()
		if err != nil {
			http.NotFound(w, r)
			return
		}

		redirectURL = clickURL
		log.Debug("ClickHandler: from redis")
	}

	http.Redirect(w, r, redirectURL, 303)

	t := time.Now()

	payload := fmt.Sprintf(`{"datetime": "%s"}`, t.UTC().Format(time.RFC3339))
	clickMessage := message{
		uuid.NewV4(),
		clickQueue,
		"persist_push_clicked",
		fmt.Sprintf(`["%s", %s]`, pushID, payload),
		t.UnixNano() / int64(time.Millisecond),
	}
	clickActor := actor{rabbitCh, clickQueue}
	err = clickActor.tell(clickMessage.serialized())
	if err != nil {
		log.Warn(err)
	}
}

func connectToRabbitMQ(uri string) *amqp.Connection {
	for {
		conn, err := amqp.Dial(uri)

		if err == nil {
			return conn
		}

		log.Println(err)
		log.Printf("Trying to reconnect to RabbitMQ at %s\n", uri)
		time.Sleep(1000 * time.Millisecond)
	}
}

func rabbitConnector(uri string) {
	var rabbitErr *amqp.Error

	for {
		rabbitErr = <-rabbitCloseError
		if rabbitErr != nil {
			log.Printf("Connecting to %s\n", uri)

			rabbitConn = connectToRabbitMQ(uri)
			rabbitCloseError = make(chan *amqp.Error)
			rabbitConn.NotifyClose(rabbitCloseError)

			rabbitCh, _ = rabbitConn.Channel()

		}
	}
}

func main() {
	var (
		err error
	)

	// Config
	err = env.Parse(&cfg)
	failOnError(err)

	// Logger
	log = logrus.New()

	if cfg.Debug {
		log.SetLevel(logrus.DebugLevel)
	}

	// REDIS
	opt, err := redis.ParseURL(cfg.RedisURL)
	failOnError(err)
	log.Debug("Redis connection established")

	redisClient = redis.NewClient(opt)

	// AEROSPIKE
	hosts := []*as.Host{}
	for _, host := range cfg.AerospikeHosts {
		hosts = append(hosts, as.NewHost(host, cfg.AerospikePort))
	}
	asClient, err = as.NewClientWithPolicyAndHost(nil, hosts...)
	failOnError(err)
	log.Debug("Aerospike connection established")

	// RABBITMQ
	rabbitCloseError = make(chan *amqp.Error)

	go rabbitConnector(cfg.RabbitMQURL)

	rabbitCloseError <- amqp.ErrClosed

	// ROUTER
	r := mux.NewRouter()

	r.Handle("/metrics", prometheus.Handler())
	r.HandleFunc(`/pushes/{push-id}/icon/`, IconHandler)
	r.HandleFunc(`/pushes/{push-id}/click/`, ClickHandler)
	r.HandleFunc(`/icon`, IconHandler2)
	r.HandleFunc(`/image`, ImageHandler2)
	r.HandleFunc(`/click`, ClickHandler2)

	n := negroni.New()
	m := negroniprometheus.NewMiddleware("push-tracker")

	n.Use(m)
	n.UseHandler(r)

	n.Run(":" + os.Getenv("PORT"))
}
