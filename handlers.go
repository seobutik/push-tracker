package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	as "github.com/aerospike/aerospike-client-go"
	"github.com/twinj/uuid"
)

// IconHandler downloads image by url and serve it to client
func IconHandler2(w http.ResponseWriter, r *http.Request) {
	vars := r.URL.Query()

	pushIDs, _ := vars["push-id"]

	var (
		pushID   = pushIDs[0]
		keyRedis = fmt.Sprintf("pushes:%s:icon-url", pushID)
		keyAs    = fmt.Sprintf("push:%s", pushID)
		iconURL  string
	)

	// Aerospike
	policy := as.NewPolicy()
	key, err := as.NewKey(cfg.AerospikeNS, cfg.AerospikeSet, keyAs)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}
	rec, err := asClient.Get(policy, key)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}

	if rec != nil {
		iconURL = rec.Bins["icon-url"].(string)
		log.Debug("IconHandler: from aerospike")
	} else {
		val, err := redisClient.Get(keyRedis).Result()
		if err != nil {
			http.NotFound(w, r)
			return
		}

		iconURL = val
		log.Debug("IconHandler: from redis")
	}

	// download content
	timeout := time.Duration(10 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	response, err := client.Get(iconURL)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", response.Header.Get("Content-Type"))
	w.Write(body)

	t := time.Now()

	payload := fmt.Sprintf(`{"datetime": "%s"}`, t.UTC().Format(time.RFC3339))
	deliveryMessage := message{
		uuid.NewV4(),
		deliveryQueue,
		"persist_push_delivered",
		fmt.Sprintf(`["%s", %s]`, pushID, payload),
		t.UnixNano() / int64(time.Millisecond),
	}
	deliveryActor := actor{rabbitCh, deliveryQueue}
	err = deliveryActor.tell(deliveryMessage.serialized())
	if err != nil {
		log.Warn(err)
	}

	postbackMessage := message{
		uuid.NewV4(),
		postbackQueue,
		"send_postback",
		fmt.Sprintf(`["%s", "delivery"]`, pushID),
		t.UnixNano() / int64(time.Millisecond),
	}
	postbackActor := actor{rabbitCh, postbackQueue}
	err = postbackActor.tell(postbackMessage.serialized())
	if err != nil {
		log.Warn(err)
	}
}

func ImageHandler2(w http.ResponseWriter, r *http.Request) {
	vars := r.URL.Query()

	pushIDs, _ := vars["push-id"]

	var (
		pushID   = pushIDs[0]
		keyRedis = fmt.Sprintf("pushes:%s:image-url", pushID)
		keyAs    = fmt.Sprintf("push:%s", pushID)
		imageURL string
	)

	// Aerospike
	policy := as.NewPolicy()
	key, err := as.NewKey(cfg.AerospikeNS, cfg.AerospikeSet, keyAs)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}
	rec, err := asClient.Get(policy, key)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}

	if rec != nil {
		imageURL = rec.Bins["image-url"].(string)
		log.Debug("ImageHandler: from aerospike")
	} else {
		val, err := redisClient.Get(keyRedis).Result()
		if err != nil {
			http.NotFound(w, r)
			return
		}

		imageURL = val
		log.Debug("ImageHandler: from redis")
	}

	// download content
	timeout := time.Duration(10 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	response, err := client.Get(imageURL)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", response.Header.Get("Content-Type"))
	w.Write(body)
}

// ClickHandler performs a redirect
func ClickHandler2(w http.ResponseWriter, r *http.Request) {
	vars := r.URL.Query()

	pushIDs, _ := vars["push-id"]

	var (
		pushID      = pushIDs[0]
		keyRedis    = fmt.Sprintf("pushes:%s:click-action", pushID)
		keyAs       = fmt.Sprintf("push:%s", pushID)
		redirectURL string
	)

	// Aerospike
	policy := as.NewPolicy()
	key, err := as.NewKey(cfg.AerospikeNS, cfg.AerospikeSet, keyAs)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}
	rec, err := asClient.Get(policy, key)
	if err != nil {
		log.Error(err)
		http.NotFound(w, r)
		return
	}

	if rec != nil {
		redirectURL = rec.Bins["click-action"].(string)
		log.Debug("ClickHandler: from aerospike")
	} else {
		clickURL, err := redisClient.Get(keyRedis).Result()
		if err != nil {
			http.NotFound(w, r)
			return
		}

		redirectURL = clickURL
		log.Debug("ClickHandler: from redis")
	}

	http.Redirect(w, r, redirectURL, 303)

	t := time.Now()

	payload := fmt.Sprintf(`{"datetime": "%s"}`, t.UTC().Format(time.RFC3339))
	clickMessage := message{
		uuid.NewV4(),
		clickQueue,
		"persist_push_clicked",
		fmt.Sprintf(`["%s", %s]`, pushID, payload),
		t.UnixNano() / int64(time.Millisecond),
	}
	clickActor := actor{rabbitCh, clickQueue}
	err = clickActor.tell(clickMessage.serialized())
	if err != nil {
		log.Warn(err)
	}
}
